<!DOCTYPE html>
<html lang="ru">
<?php header('Content-Type: text/html; charset=UTF-8');?>
<head>
  <title>Form</title>
  <meta charset="UTF-8" />
  <link rel="stylesheet" href="main.css">
</head>

<body>
  
  <form class="decor" action="" method="post">
    <div class="form-left-decoration"></div>
    <div class="form-right-decoration"></div>
    <div class="circle"><div id="heart"></div>
    <div id="infinity"></div></div>
    
    <div class="form-inner">
      <h3>Напишите нам</h3>
      <input type="text" placeholder="Попробуйте не вводить имя)" name="fio">
      <input type="email" placeholder="Email" name="email">

      <input type="date" id="date" name="date" />
      <h4>Пол:</h4>
      <div class="sex">
        <label>
          <input type="radio" name="sex" value="male">Мужской &nbsp;</label>
        <label>
          <input type="radio" name="sex" value="female">&nbsp; Женский</label>
      </div>
      <h4> Количество конечностей:</h4>
      <div class="sex">
        <label>
          <input type="radio" name="some" value="1">одна&nbsp;</label>
        <label>
          <input type="radio" name="some" value="2">&nbsp;две</label>

        <label>
          <input type="radio" name="some" value="3">&nbsp;три</label>
        <label>
          <input type="radio" name="some" value="4">&nbsp;четыре</label>
        <label>
          <input type="radio" name="some" value="5">&nbsp;пять</label>
        <label>
          <input type="radio" name="some" value="6">&nbsp;шесть</label>
      </div>

      <br>
      <div>
        <select size="4" multiple name = "abilities[]">
          <option disabled >суперсила</option>
          <option value="god">Бессмертие</option>
          <option value="idclip"> Бесформенность</option>
          <option value="Levitation">Полет</option>
        </select>
      </div>
      <br>
      <textarea name = "about" placeholder="о себе..." rows="3"></textarea>
      <div class="rul">
        <label> <input type="checkbox" name="accept">С политикой формы согласен</label></div>
      <input type="submit" value="Отправить">
    </div>
  </form>

</body>